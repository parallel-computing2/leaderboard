# Introduction

This is the explanation and the code of the leaderboard. You can submit
your implementation, after which it will be timed and written to
`/tmp/leaderboard/leaderboard.omp`
The leaderboard is updated every 10 minutes, so do not be alarmed if it does
not show up immediately.

Your submission is tested on three different data sets, of which the runtime
in seconds is printed.

# Submission

1. Import key.asc ( `gpg --import key.asc` ). (Execute the command from this directory,
   because that is where the file key.asc is located.)
2. Make sure that your implementation does not modify `main` or print anything
   other the sequential version does not.
3. Name your implementation
   `group-<group-number>-<any other identifier>-mandel.c`
4. Write your encrypted program to `/tmp/leaderboard/omp`. For example
   (do not blindly copy paste, replace the file name!):

   `gpg --output /tmp/leaderboard/omp/group-999-crazy-mandel.c --encrypt --recipient leaderboard@parallelcomputing.ru src/omp/mandelbrot.c`

# MPI (UNDER CONSTRUCTION)

Make sure to print only the fraction of points in the set to `stderr`
in format `fprintf(stderr, "%lf\n", ...);` an the GFLOPS/s to stdout, in
format `printf("%lf\n", ...);`. Nothing else! 

Then do the following steps

1. Name your implementation
   `group-<group-number>-<any other identifier>-mandel.c`
2. Write your encrypted program to `/tmp/leaderboard/mpi`. For example
   (do not blindly copy paste, replace the file name!):

   `gpg --output /tmp/leaderboard/mpi/group-999-crazy-mandel.c --encrypt --recipient leaderboard@parallelcomputing.ru src/mpi/mandelbrot.c`

# Trouble shooting

If `gpg` gives you a warning, have a look at [this](https://stackoverflow.com/questions/33361068/gnupg-there-is-no-assurance-this-key-belongs-to-the-named-user).
We use the key only for encryption, not certifying, so no need to worry.

You should do this from `cnlogin22`, not `lilo8`. In fact, you will not need
`lilo8` for this entire course.

# OpenCL

1. Name your implementation
   `group-<group-number>-<any other identifier>-mandel.c`
   and exactly the same but with `.cl` for the kernel
   `group-<group-number>-<any other identifier>-mandel.cl`
2. Make sure your program outputs ONLY the picture to `stderr` and ONLY the gflops in
   format `%lf\n` to `stdout`. So use `initGPU` and not `initVerboseGPU`.
3. In `readOpenCL`, use only the file name, with no path. 
4. Write your encrypted programs to `/tmp/leaderboard/ocl`. For example
   (do not blindly copy paste, replace the file name!):

   `gpg --output /tmp/leaderboard/ocl/group-999-crazy-mandel.c --encrypt --recipient leaderboard@parallelcomputing.ru src/ocl/mandelbrot.c
   `gpg --output /tmp/leaderboard/ocl/group-999-crazy-mandel.cl --encrypt --recipient leaderboard@parallelcomputing.ru src/ocl/mandelbrot.cl

