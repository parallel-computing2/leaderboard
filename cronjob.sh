#!/bin/sh

find /tmp/leaderboard/omp -mindepth 1 -cmin -10 | \
while IFS= read -r file
do
    name=$(basename "$file" .c)
    gpg --output "$name".c --decrypt "$file"
    sed "s/NAME/$name/g" omp_template.sh > "$name".sh
    sbatch "$name".sh
done

# Cannot be done in sbatch because /tmp is not mounted to cn132
sort -n -k 2 leaderboard_omp.txt > /tmp/leaderboard/leaderboard_omp.txt
