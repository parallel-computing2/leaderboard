#!/bin/sh

#SBATCH --account=csmpistud
#SBATCH --partition=csmpi_fpga_short
#SBATCH --cpus-per-task=32
#SBATCH --output=bench_omp.out

gcc -Ofast -march=native -mtune=native NAME.c -o NAME

{
    printf '%s, ' NAME
    ./NAME 10000 10
} >> leaderboard_omp.txt
